package tests;

import appli.Appli;
import appli.IEntity;
import entities.DroneRandom;
import entities.Treasure;
import entities.VoleurRadar;
import entities.Wall;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jonas on 27/05/15.
 */
public class VoleurRadarTest {

    @Test
    public void testMove() throws Exception {
        Appli app = new Appli(200);
        VoleurRadar v = new VoleurRadar(1,1);
        app.getAllEntities().add(new Wall(0,0));app.getAllEntities().add(new Wall(1,0));app.getAllEntities().add(new Wall(2,0));
        app.getAllEntities().add(new Wall(0,1));app.getAllEntities().add(v);            app.getAllEntities().add(new Wall(2,1));
        app.getAllEntities().add(new Wall(0,2));app.getAllEntities().add(new Wall(1,2));app.getAllEntities().add(new Wall(2,2));
        app.getMovingEntities().add(v);
        app.getStealerEntities().add(v);
        //app.getAnalyseEntities().add(v);
        // On test le fait que le VoleurRandom ne fait pas de mouvement incohérent
        // -> uniquement dans les 8 cases directement collées
        // On le teste sur plusieurs fois, du fait du Random, pour être un peu plus sur
        //v.Analyse(app);
        for (int i = 0; i<15 && v.x()==1 && v.y()==1; i++){
            v.move(app);
        }
        assertTrue(v.x()==1 && v.y()==1);

    }

    @Test
    public void testSteal() throws Exception {
        Appli app = new Appli(200);
        Treasure t1 = new Treasure(10,10, 1);   // trésor inatteignable
        Treasure t2 = new Treasure(1,1,1);      // trésor atteignable
        VoleurRadar v = new VoleurRadar(0,0);
        app.getAllEntities().add(v);app.getStealerEntities().add(v);
        app.getAllEntities().add(t1); app.getStolenEntities().add(t1);
        v.steal(app.getStolenEntities());
        assertFalse(t1.value() == 0);

        app.getAllEntities().add(t2); app.getStolenEntities().add(t2);
        v.steal(app.getStolenEntities());
        assertFalse(t1.value() == 0);
        assertTrue(t2.value()==0);
    }

    @Test
    public void testStop() throws Exception {
        Appli app = new Appli(200);
        List<IEntity> delimiteurs = app.getAllEntities();
        delimiteurs.add(new Wall(0,0));delimiteurs.add(new Wall(1,0));delimiteurs.add(new Wall(2,0));
        delimiteurs.add(new Wall(3,0));delimiteurs.add(new Wall(3,1));delimiteurs.add(new Wall(3,2));
        delimiteurs.add(new Wall(3,3));delimiteurs.add(new Wall(0,3));delimiteurs.add(new Wall(1,3));
        delimiteurs.add(new Wall(2,3));delimiteurs.add(new Wall(0,1));delimiteurs.add(new Wall(0,2));
        VoleurRadar v = new VoleurRadar(1,1);
        Treasure t = new Treasure(2,2,2);  // un trésor atteiagnable
        DroneRandom d = new DroneRandom(2,2); // le drone qui va le stoper
        app.getStolenEntities().add(t);
        v.steal(app.getStolenEntities()); // on vole le trésor
        assertTrue(app.getStolenEntities().get(0).value()==0);
        assertTrue(v.pactole()==2);
        app.getStolenEntities().remove(0);
        app.getStopedEntities().add(v);
        d.stop(app.getStopedEntities()); // on stope le voleur
        assertTrue(v.pactole()==0); // on vérifie que le voleur a payé son pot-de-vin
        v.move(app);
        assertTrue(v.x()==1 && v.y()==1); // on vérifie qu'il a pas bougé
        v.move(app);
        assertTrue(v.x()==1 && v.y()==1); // on vérifie qu'il a toujours pas bougé
        v.move(app);
        assertFalse(v.x()==1 && v.y()==1); // on vérifie que cette fois il a bougé
        // ( ce test peut être rendu faux si le random.randint renvoie toujours 1 )


    }

    @Test
    public void testIsArrested() throws Exception {
        Appli app = new Appli(200);
        VoleurRadar v = new VoleurRadar(1,1); app.getAllEntities().add(v);
        Treasure t = new Treasure(1,2,2);       app.getAllEntities().add(t);
        DroneRandom d = new DroneRandom(2,1);   app.getAllEntities().add(d);
        app.getStolenEntities().add(t);
        app.getStealerEntities().add(v);
        app.getStopedEntities().add(v);
        app.getStoperEntities().add(d);
        // on va donner un pactole au voleur en le laissant voler le trésor
        v.steal(app.getStolenEntities());
        assertTrue(v.pactole()!=0);
        // on va arréter le voleur une première fois mais il devrait pouvoir payer son pot de vin
        d.stop(app.getStopedEntities());
        assertFalse(v.isArrested());
        assertTrue(v.pactole()==0);
        // on va le stoper de nouveau mais cette fois alors qu'il n'a plus d'argent
        d.stop(app.getStopedEntities());
        assertTrue(v.isArrested()); // cette fois il est donc marqué comme "exclu"
    }

}