package appli;

import java.util.List;

/**
 * Created by jonas on 18/04/15.
 */
public interface IStoperEntity extends IEntity {
    /**
     * @bief prend la liste des entités stoppantes,si celles- ci sont dans le périmétre d'une entité stoppable, alors elles les stoppent
     * @param stopedEntities
     */
    void stop(List<IStopedEntity> stopedEntities);
}
