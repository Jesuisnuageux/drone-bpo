package appli;

import java.util.List;

/**
 * Created by jonas on 18/04/15.
 */
public interface IStealerEntity extends IEntity{
    /**
     * @brief Permet de vérifier s'il existe des entitées à détrousser et d'effectuer cette action si possible
     * @param stolenEntity liste des entitées détroussable
     */
    void steal(List<IStolenEntity> stolenEntity);

    /**
     * @brief Renvoie le pactole amassé par un voleur
     * @return le pactole amassé par un voleur
     */
    int pactole();
}
