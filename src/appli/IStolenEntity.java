package appli;

/**
 * Created by jonas on 18/04/15.
 */
public interface IStolenEntity extends IEntity {
    /**
     * @brief met la valeur du trésor dérobé à 0 et retourne la valeur du trésor
     * @return le valeur du trésor dérobé
     */
    int steal();

    /**
     * @brief retourne la valeur du trésor
     * @return
     */
    int value();
}
