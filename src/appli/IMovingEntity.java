package appli;


/**
 * Created by jonas on 18/04/15.
 */
public interface IMovingEntity extends IEntity{
    /**
     * @brief Update la position d'une entitée en fonction de l'état actuelle de la simulation ( Appli )
     * @param app l'appli ( afin d'obtenir toutes les informations dont on a besoin sur son état actuel )
     */
    void move(Appli app);
}
