package appli;

/**
 * Created by jonas on 18/04/15.
 */
public interface IStopedEntity extends IEntity {
    /** si le voleur est arrété en possession d'un pactol, ce pactol est retiré du jeu et le voleur attend
     * un delai avant de pouvoir se déplacer
     * si le voleur est arrete sans pactole, il est retiré du jeu
     */
    void stop();

    /** test si un voleur est en arret ou non
     * @return true s'il est arrété
     */
    boolean isArrested();
}
