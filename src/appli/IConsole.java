package appli;

import io.Console;

/**
 * Created by jonas on 15/04/15.
 * Décrit les interactions possibles avec la classe Console de io
 */
public interface IConsole {
    /**
     * @brief Ajoute une entitée à la classe gérant l'affichage
     * @param e l'entitée à ajouter
     * @return Retourne la classe afin de pouvoir chainer les appels -> console.draw(e1).draw(e2).draw(e3) etc..
     */
    Console draw(IEntity e);

    /**
     * @brief Reset l'état interne de l'affichage
     */
    void clear_screen();
    String toString();

    /**
     * @brief Spécifie la taille pour l'allocation des dimensions du tableau de char utilisé en interne dans la classe
     * @param size_x taille sur l'axe x
     * @param size_y taille sur l'axe y
     */
    void setSize(int size_x, int size_y);
}
