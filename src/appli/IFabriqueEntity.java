package appli;

/**
 * Created by jonas on 13/05/15.
 */
public interface IFabriqueEntity {
    /**
     * @brief Stocke une entitée dans l'Appli en fonction de son caractère d'affichage et de ses coordonnées
     * @param c caractère d'affichage de l'entitée
     * @param x composante x des coordonnées de l'entitée
     * @param y composante y des coordonnées de l'entitée
     * @param a l'appli dans laquelle l'entitée doit être stockée
     */
    void creerEntity(char c, int x, int y, Appli a);
}
