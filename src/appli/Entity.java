package appli;

/**
 * Created by jonas on 15/04/15.
 */
public abstract class Entity implements IEntity{
    protected Coord coord;

    public int x(){ return coord.getX(); }
    public int y(){ return coord.getY(); }

    public Coord getCoord(){ return coord; }
    public void setCoord(Coord coord){ this.coord = coord; }

    public Entity(int x, int y){
        this.coord = new Coord(x,y);
    }
    public Entity(Entity other){
        this.coord = new Coord(other.coord);
    }

    public abstract char toChar();

    /**
     * @brief Test que des coordonnées sont a une distance spécifiée de l'entitée appelante
     * @param x coordonnées sur x
     * @param y coordonnées sur y
     * @param range distance spécifiée
     * @return Vrai si elle est à cette distance ou une distance plus courte, Faux sinon
     */
    public boolean near(int x, int y, int range){
        return coord.near(x,y,range);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity entity = (Entity) o;

        return !(coord != null ? !coord.equals(entity.coord) : entity.coord != null);

    }

    public boolean equals(Coord coord){
        return this.coord.equals(coord);
    }

    @Override
    public int hashCode() {
        return coord != null ? coord.hashCode() : 0;
    }
}
