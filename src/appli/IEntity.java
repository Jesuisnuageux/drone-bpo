package appli;

/**
 * @brief Created by jonas on 15/04/15.
 * L'interface commune pour toutes les entitées
 */
public interface IEntity {
    /**
     * @brief Renvoie le caractère à afficher pour cette entitée
     * @return le caractère à afficher pour cette entitée
     */
    char toChar();

    /**
     * @brief Renvoie la composante sur l'axe des x des coordonnées de l'entitée
     * @return composante sur l'axe des x des coordonnées de l'entitée
     */
    int x();

    /**
     * @brief Renvoie la composante sur l'axe des y des coordonnées de l'entitée
     * @return composante sur l'axe des y des coordonnées de l'entitée
     */
    int y();

    /**
     * @brief Renvoie l'objet Coord de l'entitée
     * @return l'objet Coord de l'entitée
     */
    Coord getCoord();

    /**
     * @brief Change la valeur de l'objet Coord de l'entitée
     * @param coord nouvelle valeur
     */
    void setCoord(Coord coord);
    boolean equals(Coord coord);

    /**
     * @brief Test que des coordonnées x,y sont a une distance spécifiée de l'entitée appelante
     * @param x coordonnée sur x
     * @param y coordonnée sur y
     * @param range distance spécifiée
     * @return Vrai si elle est à cette distance ou une distance plus courte, Faux sinon
     */
    boolean near(int x, int y, int range);
}
