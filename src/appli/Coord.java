package appli;

/**
 * Created by jonas on 20/05/15.
 */
public class Coord {
    private int x,y;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coord coord = (Coord) o;
        return x == coord.x && y == coord.y;
    }

    /**
     * @brief Test que des coordonnées x,y sont a une distance spécifiée de l'entitée appelante
     * @param x coordonnée sur x
     * @param y coordonnée sur y
     * @param range distance spécifiée
     * @return Vrai si elle est à cette distance ou une distance plus courte, Faux sinon
     */
    public boolean near(int x, int y, int range){
        return Math.abs(this.x - x) <= range && Math.abs(this.y - y) <= range;
    }

    /**
     * @brief Test que des coordonnées sont a une distance spécifiée de l'entitée appelante
     * @param coord coordonnée à tester
     * @param range distance spécifiée
     * @return Vrai si elle est à cette distance ou une distance plus courte, Faux sinon
     */
    public boolean near(Coord coord, int range){
        return this.near(coord.x,coord.y,range);
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public Coord(Coord other){
        this.x = other.x;
        this.y = other.y;
    }
    public Coord(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){ return this.x; }
    public int getY(){ return this.y; }

    public Coord setX(int x){ return new Coord(x, this.y); }
    public Coord setY(int y){ return new Coord(this.x, y); }

}
