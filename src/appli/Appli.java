package appli;


import entities.FabriqueEntity;
import io.Console;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Appli{
    private List<IEntity> allEntities ;
    private List<IMovingEntity> movingEntities ;
    private List<IStealerEntity> stealerEntities ;
    private List<IStolenEntity> stolenEntities;
    private List<IStoperEntity> stoperEntities ;
    private List<IStopedEntity> stopedEntities ;
    private IConsole console;
    private IFabriqueEntity fabrique;
    //private int timeout = 50; // delais d'affichage en ms -> 50fps ou 60fps, je me souviens plus
    private int timeout = 2000; // slower fps -> debug purpose

     public Appli(int timeout){
         this.timeout = timeout;
         this.allEntities = new ArrayList<IEntity>();
         this.movingEntities = new ArrayList<IMovingEntity>();
         this.stealerEntities = new ArrayList<IStealerEntity>();
         this.stolenEntities = new ArrayList<IStolenEntity>();
         this.stoperEntities = new ArrayList<IStoperEntity>();
         this.stopedEntities = new ArrayList<IStopedEntity>();
         this.console = Console.getConsole(0, 0);
         this.fabrique = FabriqueEntity.getFabrique();

     }

    public List<IEntity> getAllEntities(){
        return allEntities;
    }
    public List<IMovingEntity> getMovingEntities(){
        return movingEntities;
    }
    public List<IStealerEntity> getStealerEntities(){
        return stealerEntities;
    }
    public List<IStolenEntity> getStolenEntities(){
        return stolenEntities;
    }
    public List<IStoperEntity> getStoperEntities(){
        return stoperEntities;
    }
    public List<IStopedEntity> getStopedEntities(){
        return stopedEntities;
    }
    public IConsole getConsole(){return console;}
    //ajouter les liste en private dans appli
    //pas encore testée

    /**
     * @brief Fonction de lecture d'un fichier qui stocke dans l'Appli les entités présentes
     * @param nom   Nom du fichier à lire
     * @throws FileNotFoundException    Si le fichier est introuvable
     */
    public  void lecture( String nom) throws FileNotFoundException {
        int longueur,largeur,i,j;
        String s;
        Scanner in = new Scanner (new FileInputStream(nom) ) ;
        // initialise la console
        longueur= in.nextInt();
        largeur=in.nextInt();
        this.console.setSize(longueur,largeur);
        in.nextLine();
        //initialise les entitées differentes de ' '
        for (j = 0; j < largeur && in.hasNextLine(); ++j) {
            s = in.nextLine();
            for (i = 0; i < longueur && i < s.length(); ++i) {
                if(s.charAt(i)!=' ')
                    fabrique.creerEntity(s.charAt(i), i, j, this);
                //fabrique les entités en fonction du char et de la position dans la matrice
            }
        }
        //System.out.println(in.nextLine( ) ) ;
        in.close() ;
    }

    /**
     * @brief Fonction qui va simuler des tours de "jeu" tant que la condition de fin de simulation n'est pas vrai
     */
    public void run(){

        this.print();


        while( !this.ended() ){
            System.out.println("----------------------------------------------------");
            this.update();
            this.garbageCollector();
            this.print();
            this.delay();
        }
    }

    // -- Sous fonction de run() -- //

    /**
     * @brief Fais faire un mouvement à chaque entités puis demande aux entitées qui volent de voler et au entités qui stopent
     *      de stoper
     */
    private void update(){
        for (IMovingEntity e: movingEntities)  {    e.move(this);               }
        for (IStealerEntity e: stealerEntities){    e.steal(stolenEntities);    }
        for (IStoperEntity e: stoperEntities)  {    e.stop(stopedEntities);     }
    }

    /**
     * @brief Affiche sur stdout un aperçu de l'état de la simulation
     */
    private void print(){
        console.clear_screen();
        for (IEntity e: allEntities){ console.draw(e); }
        System.out.println(console);
    }

    /**
     * @brief Supprime les Treasures déjà ramassé et les voleurs qui n'ont pas pu payer de pot-de-vin
     */
    private void garbageCollector(){
        // on enlève les trèsors volés
        for (int i = 0; i < stolenEntities.size();) {
            if (stolenEntities.get(i).value()==0){
                allEntities.remove(allEntities.indexOf(stolenEntities.get(i)));
                stolenEntities.remove(i);
            }else{
                i++;
            }
        }
        // on enlève les voleurs arrétés
        for (int i = 0; i < stopedEntities.size();){
            if (stopedEntities.get(i).isArrested()){
                allEntities.remove(allEntities.indexOf(stopedEntities.get(i)));
                stealerEntities.remove(stealerEntities.indexOf(stopedEntities.get(i)));
                movingEntities.remove(movingEntities.indexOf(stopedEntities.get(i)));
                stopedEntities.remove(i);
            }else{
                i++;
            }
        }
    }

    /**
     * @brief Permet d'attendre entre chaque affichage afin de ne pas flooder la console
     */
    private void delay(){
        try { Thread.sleep(this.timeout); } catch (InterruptedException e) { e.printStackTrace(); }
    }

    /**
     * @brief Condition d'arrêt de la simulation
     * @return Vrai s'il faut s'arrêter, Faux sinon
     */
    private boolean ended(){
        return this.stealerEntities.size()==0 || this.stolenEntities.size()==0;
    }
}