package Moves;

import appli.IEntity;
import entities.IMove;
import pathUtils.pathEntity;
import pathUtils.pathFinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonas on 20/05/15.
 */
public class MoveFollowingPath implements IMove{
    private pathFinder pather;
    public MoveFollowingPath(pathFinder pather){
        this.pather = pather;
    }

    public void move(IEntity entity, List<IEntity> allEntities){
        entity.setCoord(pather.getNextStep().getCoord());
    }
}
