package Moves;

import appli.Coord;
import appli.IEntity;
import entities.IMove;

import java.util.List;
import java.util.Random;

/**
 * Created by jonas on 20/05/15.
 */
public class MoveRandom implements IMove {

    public Random rand;

    public MoveRandom(){
        this.rand = new Random();
    }

    public void move(IEntity entity, List<IEntity> allEntities){
        int x = entity.x();
        int y = entity.y();
        int cpt = 0;
        // possible boucle infini .. mais en vrai .. ça paaaaasse :3
        while (x==entity.x()&&y==entity.y() && cpt<15){
            x = entity.x()+rand.nextInt(3)-1;
            y = entity.y()+rand.nextInt(3)-1;
            cpt++;
        }
        Coord c = new Coord(x,y);
        for (IEntity ref : allEntities){
            if (ref.equals(c)){
                return;
            }
        }
        entity.setCoord(c);
    }
}
