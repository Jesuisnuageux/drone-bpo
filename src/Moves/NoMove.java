package Moves;

import appli.IEntity;
import entities.IMove;

import java.util.List;

/**
 * Created by jonas on 23/05/15.
 */
public class NoMove implements IMove {
    @Override
    public void move(IEntity entity, List<IEntity> allEntities) {
        // ne fait aucun mouvement
        // créée dans un but de debug
    }
}
