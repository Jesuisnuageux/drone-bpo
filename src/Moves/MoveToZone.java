package Moves;

import appli.Coord;
import appli.IEntity;
import entities.DroneRadarZone;
import entities.IMove;

import java.util.List;

/**
 * Created by jonas on 20/05/15.
 */
public class MoveToZone implements IMove {
    public MoveToZone() {
    }
    public void move(IEntity entity, List<IEntity> allEntities){
        /**
         *      ! ! ! IMPLÉMENTATION SALE ! ! ! ATTENTION LES YEUX ! ! !
         */
        DroneRadarZone d;
        try {
            d = (DroneRadarZone) entity;
        }catch (Exception e){
            System.err.println("[MESSAGE DU DEV'] : CETTE PARTIE EST MAL CODÉE, DÉSOLÉ");
            return;
        }
        int x,y;
        x= d.x()<d.xZone ? d.x()+1 : d.x()-1;
        y= d.y()<d.yZone ? d.y()+1 : d.y()-1;
        entity.setCoord(new Coord(x,y));

    }
}
