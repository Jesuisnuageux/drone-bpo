package entities;

import appli.*;
import pathUtils.pathFinder;


/**
 * Created by Naomie on 21/04/2015.
 */

public class VoleurRadar extends ConceptOfVoleur{
        private pathFinder<IStolenEntity> pathGen;

        public VoleurRadar(int x, int y) {
            super(x, y);
            this.delay=0;
            this.pathGen = new pathFinder();
        }

    public String printPath(){
        return this.pathGen.toString();
    }

    public VoleurRadar(VoleurRadar v) {
        super(v);
        this.pathGen = new pathFinder();
    }

    @Override
    public char toChar() {
        return 'V';
    }

    private void Analyse(Appli app){
        if (pathGen.checkIfPathIsCorrect(this, app.getAllEntities(),app.getStolenEntities())){
            return;
        }else{
            this.mover = pathGen.computeNewOptimalPath(this,app.getAllEntities(),app.getStolenEntities());
            return;
        }
    }

    @Override
    public void move(Appli app) {
        this.Analyse(app);
        if (delay<=0){
            mover.move(this, app.getAllEntities());
        }else{
            delay--;
        }
    }
}