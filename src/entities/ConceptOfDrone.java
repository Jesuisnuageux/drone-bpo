package entities;

import appli.*;

import java.util.List;

/**
 * Created by jonas on 27/05/15.
 */
public abstract class ConceptOfDrone extends Entity implements IMovingEntity, IStoperEntity{
    protected IMove mover;

    public ConceptOfDrone(int x, int y) {
        super(x, y);
    }

    public ConceptOfDrone(Entity other) {
        super(other);
    }

    /**
     * @param stopedEntities prend la liste des entités stoppantes,
     */
    @Override
    public void stop(List<IStopedEntity> stopedEntities) {
        for (IStopedEntity e : stopedEntities){
            if (e.near(x(),y(),1) && !e.hasDelay()) {
                e.stop();
            }
        }
    }
}
