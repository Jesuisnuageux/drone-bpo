package entities;

import appli.Appli;
import appli.IFabriqueEntity;

/**
 * Created by Naomie on 19/04/2015.
 */
public class FabriqueEntity implements IFabriqueEntity {

    private static volatile FabriqueEntity luniquefabrique = null;

    private FabriqueEntity(){
        super();
    }

    public final static IFabriqueEntity getFabrique(){
        if (luniquefabrique==null){
            luniquefabrique = new FabriqueEntity();
        }
        return luniquefabrique;
    }

    // analyse le caractère reçut du fichier texte
    public void creerEntity(char c, int x, int y, Appli a){
        if (c =='v' ) {
            VoleurRandom v= new VoleurRandom(x,y);
            a.getMovingEntities().add(v);
            a.getAllEntities().add(v);
            a.getStealerEntities().add(v);
            a.getStopedEntities().add(v);
           }
        else if (c=='V'){
            VoleurRadar v= new VoleurRadar(x,y);
            a.getMovingEntities().add(v);
            a.getAllEntities().add(v);
            a.getStealerEntities().add(v);
            a.getStopedEntities().add(v);
        }

        else if (c=='X') {
            Wall w = new Wall(x, y);
            a.getAllEntities().add(w);
        }
        else if (c=='d'){
            DroneRandom d= new DroneRandom(x,y);
            a.getAllEntities().add(d);
            a.getMovingEntities().add(d);
            a.getStoperEntities().add(d);
        }
        else if(c=='D'){
            DroneRadar d=new DroneRadar(x,y);
            a.getAllEntities().add(d);
            a.getMovingEntities().add(d);
            a.getStoperEntities().add(d);
        }
        else if (Integer.parseInt(""+c)>=1 && Integer.parseInt(""+c)<=9) {
            Treasure t= new Treasure(x,y,Integer.parseInt(""+c));
            a.getAllEntities().add(t);
            a.getStolenEntities().add(t);
        }
        else throw new RuntimeException("Valeur d'entité incorrecte! "+c+"("+x+";"+y+")");

    }

}
