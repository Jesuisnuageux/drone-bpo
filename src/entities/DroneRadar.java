package entities;

import appli.*;
import pathUtils.pathFinder;

public class DroneRadar extends ConceptOfDrone{
    private IMove mover;
    private pathFinder<IStopedEntity> pathGen;
    //private static final int distanceAnalyse=2;
    public DroneRadar(int x, int y) {
        super(x, y);
        this.pathGen = new pathFinder<IStopedEntity>();
    }

    @Override
    public char toChar() {
        return 'D';
    }


    private void Analyse(Appli app) {
        if (pathGen.checkIfPathIsCorrect(this,app.getAllEntities(),app.getStopedEntities())) {
            return;
        }else {
            this.mover = pathGen.computeNewOptimalPath(this, app.getAllEntities(), app.getStopedEntities());
            return;
        }
    }

    @Override
    public void move(Appli app){
        this.Analyse(app);
        this.mover.move(this, app.getAllEntities());
    }
}
