package entities;

import Moves.MoveRandom;
import appli.*;

/**
 * Created by jonas on 15/04/15.
 */
public class DroneRandom extends ConceptOfDrone{
    public DroneRandom(int x, int y) {
        super(x, y);
        this.mover = new MoveRandom();
    }

    public DroneRandom(DroneRandom other){
        super(other.x(), other.y());
        this.mover = new MoveRandom();
    }

    @Override
    public void move(Appli app) {
        mover.move(this, app.getAllEntities());
    }

    @Override
    public char toChar() {
        return 'd';
    }

}
