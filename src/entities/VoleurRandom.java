package entities;

import Moves.MoveRandom;
import appli.*;

/**
 * Created by jonas on 15/04/15.
 */
public class VoleurRandom extends ConceptOfVoleur{
    public VoleurRandom(int x, int y) {
        super(x, y);
        //mover = new MoveRandom();
        mover = new MoveRandom();
        this.delay=0;
    }

    @Override
    public char toChar() {
        return 'v';
    }

    @Override
    public void move(Appli app) {
        if (delay<=0){
            mover.move(this, app.getAllEntities());
        }else{
            delay--;
        }
    }

}
