package entities;

import Moves.MoveRandom;
import Moves.MoveToZone;
import appli.*;

/**
 * Created by jonas on 20/05/15.
 */
public class DroneRadarZone extends ConceptOfDrone{
    public int xZone, yZone, xTailleZone, yTailleZone;
    private IMove mover;
    public DroneRadarZone(int xEntity, int yEntity, int xZone, int yZone, int xTailleZone, int yTailleZone) {
        super(xEntity, yEntity);
        this.xZone=xZone; this.yZone=yZone;
        this.xTailleZone=xTailleZone; this.yTailleZone=yTailleZone;
    }

    public DroneRadarZone(Entity other) {
        super(other);
    }

    @Override
    public char toChar() {
        return 0;
    }

    @Override
    public void move(Appli app) {
        if (this.inDatZone()){
            // on est dans la zone
            mover = new MoveRandom();
        }else{
            // on est pas dans la zone
            mover = new MoveToZone();
        }
        mover.move(this, app.getAllEntities());
    }

    private boolean inDatZone(){
        return xZone<=this.x() && this.x()<=xZone+xTailleZone && yZone<=this.y() && this.y()<=yZone+yTailleZone;
    }
}
