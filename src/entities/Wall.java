package entities;

import appli.Entity;
import appli.IEntity;

/**
 * Created by jonas on 15/04/15.
 */
public class Wall extends Entity implements IEntity {
    public Wall(int x, int y) {
        super(x, y);
    }

    @Override
    public char toChar() {
        return 'X';
    }
}
