package entities;

import appli.IEntity;

import java.util.List;

/**
 * Created by jonas on 20/05/15.
 */
public interface IMove {
    /**
     * @bief permet de generer le mouvement d'une entité en fonction de sa classe et des autres entités environnentes
     * @param entity  : prend l'entité a déplacer
     * @param allEntities : et la liste des entités presentes en jeu
     */
    void move(IEntity entity, List<IEntity> allEntities);
}
