package entities;

import appli.Entity;
import appli.IStolenEntity;


/**
 * Created by jonas on 15/04/15.
 */
public class Treasure extends Entity implements IStolenEntity {
    private int value;
    public Treasure(int x, int y, int value) {
        super(x, y);
        if (value>=10 || value<0){ throw new RuntimeException("Valeur d'un trésor incorrect !");
        }else{ this.value = value; }
    }

    @Override
    public char toChar() {
        return Integer.toString(value).charAt(0);
    }

    public int value(){ return value; }

    @Override
    public int steal() {
        int tmp = value;
        this.value=0;
        System.out.println("[info] > Le Trésor("+this.x()+","+this.y()+") a été dérobé. Il avait une valeur de "+tmp+".");
        return tmp;
    }
}
