package entities;

import appli.*;

import java.util.List;

/**
 * Created by jonas on 27/05/15.
 */
public abstract class ConceptOfVoleur extends Entity implements IMovingEntity, IStopedEntity, IStealerEntity{
    protected int pactole;
    protected int delay;
    protected boolean arrested;
    protected IMove mover;

    public ConceptOfVoleur(int x, int y) {
        super(x, y);
        pactole = 0;
        delay = 0;
    }

    public ConceptOfVoleur(Entity other) {
        super(other);
        pactole = 0;
        delay = 0;
    }

    @Override
    public void steal(List<IStolenEntity> stolenEntity) {
        for(IStolenEntity e: stolenEntity){
            if (e.near(this.x(), this.y(), 1)){
                this.pactole+=e.steal();
            }
        }
    }

    @Override
    public int pactole() {
        return pactole;
    }

    @Override
    public void stop() {
        if (this.pactole != 0) {
            this.delay = this.pactole;
            this.pactole = 0;
            System.out.println("[info] > Voleur(" + this.x() + "," + this.y() + ") a été attrapé. Durée de l'arrêt : " + this.delay + " itérations.");
        }else{
            arrested = true;
            System.out.println("[info] > Voleur(" + this.x() + "," + this.y() + ") a été attrapé sans possessions, il est exclu du jeu");
        }
    }

    public boolean isArrested(){
        return this.arrested;
    }
}
