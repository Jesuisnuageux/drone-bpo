import appli.Appli;

import java.io.FileNotFoundException;

/**
 * Created by jonas on 21/04/15.
 */
public class main {
    public static void main(String[] args){
        String filename = "test.file";
        Appli app = new Appli(500);
        try { app.lecture(filename); } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        app.run();
    }
}
