package io;

import appli.IConsole;
import appli.IEntity;

/**
 * Created by jonas on 14/04/15.
 */
public class Console implements IConsole {
    private static Console console;
    private int size_x;
    private int size_y;
    private char[][] screen;

    private Console(int size_x, int size_y){
        this.size_x=size_x;
        this.size_y=size_y;
        this.screen = new char[this.size_y][this.size_x];
        this.clear_screen();
    }

    public final static Console getConsole(int x, int y){
        if (console==null){
            console = new Console(x,y);
        }
        return console;
    }

    public void setSize(int size_x, int size_y){
        this.size_x=size_x;
        this.size_y=size_y;
        this.screen = new char[this.size_y][this.size_x];
        this.clear_screen();
    }

    public void clear_screen(){
        for (int i = 0; i < size_y; i++) {
            for (int j = 0; j < size_x; j++) {
                screen[i][j]=' ';
            }
        }
    }

    public Console draw(IEntity e){
        if (e.x()>=size_x || e.y()>=size_y || e.x()<0 || e.y()<0){
            throw new RuntimeException("Entity coord can't be print of this console :" +
                    "E("+e.x()+";"+e.y()+") not in ("+size_x+";"+size_y+")");
        }else{
            screen[e.y()][e.x()]=e.toChar();
        }
        return this;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size_y; i++) {
            sb.append(new String(screen[i])).append("\n");
        }
        //return this.gen_many_newline(10)+sb.toString();
        return sb.toString();
    }

    private String gen_many_newline(int n){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append("\n");
        }
        return sb.toString();
    }
}
