package pathUtils;

import Moves.MoveFollowingPath;
import Moves.MoveRandom;
import appli.Coord;
import appli.IEntity;
import entities.IMove;

import java.util.*;


/**
 * Created by jonas on 02/05/15.
 */
public class pathFinder<T extends IEntity> {
    // 'T' représente le type des entités recherchées ( IStolenEntity ou IstopedEntity du coup )
    private ArrayList<pathEntity> path;

    public pathFinder() {
        this.path = new ArrayList<pathEntity>();
    }

    public pathEntity getNextStep() {
        if (path.isEmpty()) {
            throw new RuntimeException("No path calculated yet");
        }
        pathEntity tmp = path.get(0);
        path.remove(0);
        return tmp;
    }

    public boolean checkIfPathIsCorrect(IEntity appelant, List<IEntity> allEntities, List<T> allTrackableEntities) {
        boolean finalPointIsCorrect = false;
        // Test pour savoir si le chemin est déjà calculé
        if (path.isEmpty()) {
            return false;
        }

        // Test pour savoir si le bout du chemin est toujours valide
        pathEntity finalPoint = path.get(path.size() - 1);
        for (T trackable : allTrackableEntities) {
            finalPointIsCorrect = finalPointIsCorrect || trackable.near(finalPoint.x(), finalPoint.y(), 1);
        }
        if (!finalPointIsCorrect) {
            return false;
        }

        // Test pour savoir si le chemin est encore utilisable
        for (pathEntity step : path) {
            for (IEntity entity : allEntities) {
                if (entity != appelant && step.equals(entity.getCoord())) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setNextStep(int x, int y) {
        while (!path.isEmpty()) {
            path.remove(0);
        }
        path.add(new pathEntity(x, y));
    }

    public IMove computeNewOptimalPath(IEntity appelant, List<IEntity> allEntities, List<T> allTrackableEntities) {
        // -- DECLARATIONS --

        // liste des positions à ne pas parcourir
        ArrayList<pathEntity> trash = new ArrayList<pathEntity>();

        // liste des chemins a évalué
        ArrayList<ArrayList<pathEntity>> currentPaths = new ArrayList<ArrayList<pathEntity>>();

        // liste des chemins à évalué au rang n+1
        ArrayList<ArrayList<pathEntity>> nextPaths = currentPaths;  // on initialise nextPaths avec currentPaths pour patcher
                                                                    // la condition d'arret de l'itération au rang 0

        // -- INITIALISATION --

        // initialisation des position à ne pas parcourir
        for (IEntity e : allEntities){ trash.add(new pathEntity(e)); }

        // initialisation des chemins à évalué au rang 0 et ajout des positions qui les composent dans trash
        {// (blocs pour limité la portée de cpt et s'assurer que le garbage collector de la jvm libère la mémoire)
            int cpt = 0;
            boolean shouldWeAddIt;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    shouldWeAddIt = true;
                    for(IEntity trashElem : trash) {
                        if (trashElem.equals(new Coord(appelant.x() + i, appelant.y() + j))) {
                            shouldWeAddIt = false;
                        }
                    }
                    if (shouldWeAddIt){
                        currentPaths.add(cpt, new ArrayList<pathEntity>());
                        currentPaths.get(cpt).add(new pathEntity(appelant.x() + i, appelant.y() + j));
                        trash.add(new pathEntity(appelant.x() + i, appelant.y() + j));
                        cpt++;
                    }
                }
            }
        }

        // -- ITERATION --

        while (!nextPaths.isEmpty()){    // on s'arrète s'il n'y a plus de nouveau chemins à parcourir
            // et qu'on a rien trouvé, on arrête les frais

            // test pour voir si les chemins actuels permettent de rejoindre une entité trackable
            {// (block pour s'assurer que le garbage collector de la jvm va désallouer currentLastPathEntity)
                int cpt=0;
                nextPaths = new ArrayList<ArrayList<pathEntity>>();
                pathEntity currentLastPathEntity; // (dernière position du chemin observé)
                pathEntity newLastPathEntityOfPAth; // (position a un déplacement de la dernière position du chemin observé)
                for (ArrayList<pathEntity> path : currentPaths) {
                    currentLastPathEntity = path.get(path.size()-1);
                    for(T trackableEntity: allTrackableEntities){
                        if (currentLastPathEntity.near(trackableEntity.x(), trackableEntity.y(), 1)){
                            // la dernière position du chemin considéré est proche d'une entité trackable
                            // on enregistre et on retourne le fait qu'il existe un chemin
                            this.path = path;
                            /** System.err.println("Nouveau chemin trouvé"); */
                            return new MoveFollowingPath(this);
                        }
                    }

                    // du coup c'es pas ce chemin qui est le bon, on va itérer au cas où
                    for (int i = -1; i <= 1; i++) {
                        for (int j = -1; j <= 1; j++) {
                            // on génére les positions où on peut se déplacer avec A déplacement de plus
                            newLastPathEntityOfPAth = new pathEntity(currentLastPathEntity.x()+i,currentLastPathEntity.y()+j);
                            // on vérifie qu'il est possible de s'y déplacer
                            for (IEntity trashElem : trash){
                                if (newLastPathEntityOfPAth.equals(new Coord(trashElem.x(), trashElem.y()))){
                                    newLastPathEntityOfPAth=null;
                                    break;
                                }
                            }
                            if (newLastPathEntityOfPAth!=null){ // si oui alors on ajoute un nouveau chemin
                                // à la liste des chemins du rang n+1
                                nextPaths.add(cpt,new ArrayList<pathEntity>());
                                int cpt2 = 0; // (position courante dans le chemin)
                                for(pathEntity pathElem : path){
                                    nextPaths.get(cpt).add(cpt2,pathElem);
                                    cpt2++;
                                }
                                nextPaths.get(cpt).add(cpt2,newLastPathEntityOfPAth);
                                cpt++;

                                // et on n'oublie PAS de l'ajouter  à la liste des positions qui ne sont plus parcourable
                                trash.add(newLastPathEntityOfPAth);
                            }
                        }
                    }
                }
            }
            // fin de l'itération, on fait n<-n+1
            currentPaths = nextPaths;
        }
        // si jamais il n'est plus possible de parcourir de nouvelle position et qu'aucun chemin ne permet de
        // rejoindre une entité trackable alors on retourne le fait qu'il n'existe pas de chemin
        /** System.err.println("Impossible de trouver un chemin"); */
        return new MoveRandom();
    }

    public String toString(){
        StringBuilder sb = new StringBuilder("[ ");
        for (pathEntity e : path){
            sb.append("(").append(e.x()).append(",").append(e.y()).append(") ");
        }
        sb.append("]");
        return sb.toString();
    }

}