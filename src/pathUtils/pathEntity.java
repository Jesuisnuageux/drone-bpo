package pathUtils;

import appli.Entity;
import appli.IEntity;

/**
 * Created by jonas on 02/05/15.
 */
public class pathEntity extends Entity {

    public pathEntity(int x, int y) {
        super(x, y);
    }

    public pathEntity(IEntity other) {
        super(other.x(), other.y());
    }

    public void set_x(int x){ this.setCoord(this.coord.setX(x));}
    public void set_y(int y){ this.setCoord(this.coord.setY(y));}

    @Override
    public char toChar() { return '.'; }

}
