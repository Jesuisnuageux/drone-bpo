# Projet de BPO : Simulation de drone de surveillance #

Réalisation : N.Fournié et J.Faure

Il s'agit d'un projet réalisé dans le cadre d'un enseignement de l'IUT Paris-Descartes.
Le but était de réalisé une simulation permettant de tester des stratégies de surveillance utilisant des drones
pour attraper des voleurs.